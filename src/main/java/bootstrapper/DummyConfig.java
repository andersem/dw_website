package bootstrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yammer.dropwizard.config.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DummyConfig extends Configuration{
    @NotNull
    private String whatever;

    @JsonProperty
    public String getWhatever() {
        return whatever;
    }
}
