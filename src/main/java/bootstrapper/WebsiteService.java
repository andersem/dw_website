package bootstrapper;


import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.assets.AssetsBundle;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import dwresources.WebsiteResource;

public class WebsiteService extends Service<DummyConfig> {
    @Override
    public void initialize(Bootstrap<DummyConfig> configBootstrap) {
        configBootstrap.addBundle(new AssetsBundle()); //For å få js og css
    }

    @Override
    public void run(DummyConfig config, Environment environment) throws Exception {
        environment.addResource(WebsiteResource.class);
    }

    public static void main(String[] args) throws Exception {
        new WebsiteService().run(args);
    }
}
